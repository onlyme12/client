import Vue from 'vue';
import App from './components/App.vue';
import VueRouter from 'vue-router';
import Welcome from './components/Welcome.vue'
import CV from './components/CV.vue'


Vue.use(VueRouter)

/**
 * Regular Expresion IndexOf for Arrays
 * This little addition to the Array prototype will iterate over array
 * and return the index of the first element which matches the provided
 * regular expresion.
 * Note: This will not match on objects.
 * @param  {RegEx}   rx The regular expression to test with. E.g. /-ba/gim
 * @return {Numeric} -1 means not found
 */
if (typeof Array.prototype.reIndexOf === 'undefined') {
    Array.prototype.reIndexOf = function (rx) {
        for (var i in this) {
            if (this[i].toString().match(rx)) {
                return i;
            }
        }
        return -1;
    };
}


const routes = [
  {path:'/welcome', component: Welcome},
  {path:'/cv', component: CV},
  { path: '*', redirect: '/welcome' }

    /*
    { path: '/home', component: Home, beforeEnter: checkLogin },
    { path: '/login', component: Login },
    { path: '/waterlevel',component: WaterLevels, beforeEnter: checkLogin },
    { path: '/user-admin',component: UserAdmin, beforeEnter: checkLogin },
    { path: '/tasking',component: Tasking, beforeEnter: checkLogin },
    */
]

export const router = new VueRouter({
  routes
})

const app = new Vue({
  el: '#app',
  router: router,
  render: h => h(App)
})
