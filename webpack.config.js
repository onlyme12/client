module.exports = {
  // the main entry of our app
  entry: {
    app: './src/index.js'
//    'webpack-dev-server/client?http://0.0.0.0:80'
  },
  // output configuration
  output: {
      path: __dirname + '/build/',
      publicPath: 'build/',
      filename: 'build.js'
  },

  // how modules should be transformed
  module: {
    loaders: [
      // process *.vue files using vue-loader
      { test: /\.vue$/,
        loader: 'vue-loader'
      },
      // process *.js files using babel-loader
      // the exclude pattern is important so that we don't
      // apply babel transform to all the dependencies!
      { test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/
      },
        {
            test: /\.(png|jpg|gif|svg)$/,
            loader: 'file-loader',
            options: {
                name: '[name].[ext]?[hash]'
            }
        },
        { test: /\.css$/, loader: 'style-loader!css-loader' },
    ]
  },
  devtool: '#eval-source-map',
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js'
        }
    },
    //,
    /*
    devServer:{
      inline:true,
      host:"127.0.0.1",
      port:8000
    }*/
  // configure babel-loader.
  // this also applies to the JavaScript inside *.vue files
  // I have moved this to .babelrc file outside of webpack.config.js
}
